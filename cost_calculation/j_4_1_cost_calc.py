"""
This job calculates the cost of implementing reduction technologies to abate vented and incomplete-flared emissions.
"""
from typing import List

import pandas as pd
import numpy as np
import numpy_financial as npf
from pathlib import Path

from cost_calculation.common.common.common import load_config
from cost_calculation.common.common.constants import *


# Reduction Technologies Applicability
def unpivot_redtech_applicability_by_segments(df: pd.DataFrame) -> pd.DataFrame:
    # Unpivot
    df = df.melt(id_vars=df.columns[:2],
                 value_vars=df.columns[-8:],
                 var_name='production_source',
                 value_name='applicability')

    return df


def process_cost_and_applicability_factor(srcpath: Path = None) -> pd.DataFrame:
    # Extract applicability factors from csv file
    df_applicability = unpivot_redtech_applicability_by_segments(
        pd.read_csv(srcpath / 'redtechApplicability.csv', sep=';', header=0))

    # Extract reduction technology costs from csv file
    df = pd.read_csv(srcpath / 'redtechGuide.csv', sep=';', header=0)

    # Merge the two previous datasets together
    df_m = df.merge(df_applicability, on=['redtech', 'reason'], how='left')

    return df_m[df_m['applicability'] != 0]


def pipeline(cfg_files: List[Path]) -> None:

    config = load_config(cfg_files)

    # File system
    # path to assumptions stored in csv file
    path_to_assumptions_data = Path(config['FILESYSTEM']['assumptions_folder'])
    # path to pre/processed folders
    path_to_preprocessed_data = Path(config['FILESYSTEM']['preprocessed_folder'])
    path_to_processed_data = Path(config['FILESYSTEM']['processed_folder'])

    # Load parameters
    df_parameters = pd.read_csv(path_to_assumptions_data / 'parameters.csv', sep=';', header=0)
    dict_parameters = dict(zip(df_parameters.parameter_name, df_parameters.parameter_value))
    discount_rate = float(dict_parameters['discount_rate'])
    methane_content = float(dict_parameters['ch4_at_well_ratio'])

    # Extract emissions by reduction technologies from csv file
    df_ch4_abated = pd.read_csv(path_to_processed_data / '3_b_abated_emissions.csv', sep=';', header=0)

    # Remove null values
    df_ch4_abated = df_ch4_abated[df_ch4_abated['baseline_emission'] > 0.0].reset_index(drop=True)

    # Retrieve reduction technology associated costs and applicability factors
    df_ch4_abated = df_ch4_abated.merge(process_cost_and_applicability_factor(path_to_assumptions_data),
                                        left_on=['reduction_tech_name', 'Production source', 'Reason'],
                                        right_on=['redtech', 'production_source', 'reason'],
                                        how='inner')
    # Remove unused columns
    df_ch4_abated.drop(columns=['redtech', 'production_source', 'reason'], inplace=True)

    # Extract national infos from csv file
    df_ch4_abated = df_ch4_abated.merge(
        pd.read_csv(path_to_assumptions_data / 'countryGuide.csv', sep=';', header=0)[
            ['countries_name', 'regions_name', 'tariff_scaler', 'labour']].drop_duplicates(),
        left_on=['Region'],
        right_on=['countries_name'],
        how='inner')

    # Remove unused columns
    df_ch4_abated.drop(columns=['countries_name'], inplace=True)

    # Extract regional infos from csv file
    df_ch4_abated = df_ch4_abated.merge(
        pd.read_csv(path_to_assumptions_data / 'regionGuide.csv', sep=';', header=0)[
            ['regions_name', 'capital_scaler', 'base', 'solar_factor']],
        on='regions_name',
        how='left')

    # If the reduction tech is solar dependant, the applicability factor will be replaced by the solar factor
    df_ch4_abated.loc[df_ch4_abated['is_solar_tech'] == True, 'applicability'] = df_ch4_abated['solar_factor']

    # reduction_ratio == efficiency?
    df_ch4_abated['recovered'] = df_ch4_abated['baseline_emission'] * df_ch4_abated['reduction_ratio'] * df_ch4_abated[
        'applicability']

    # Capital costs
    df_ch4_abated['capital'] = np.where(
        df_ch4_abated['is_imported'], df_ch4_abated['base_capital'] * (1 + df_ch4_abated['tariff_scaler'] / 100),
                                      df_ch4_abated['base_capital'] * df_ch4_abated['capital_scaler'])

    # Remove unused columns
    df_ch4_abated.drop(columns=['tariff_scaler', 'capital_scaler'], inplace=True)

    # Labour costs
    df_ch4_abated['opex'] = df_ch4_abated['base_operating'] * df_ch4_abated['labour']

    # Extract gas prices from csv file
    df_ch4_abated = df_ch4_abated.merge(
        pd.read_csv(path_to_preprocessed_data / '3_avg_gas_prices.csv', sep=';', header=0)[
            ['regions_name', 'gas_prices_usd_per_mbtu']],
        on='regions_name',
        how='inner')

    # Annual costs to finance the reduction technology + opex costs + revenue if the gas is salable
    df_ch4_abated['annual_dollars'] = np.where(df_ch4_abated['is_gas_salable'] == True,
                                               -npf.pmt(discount_rate,
                                                        df_ch4_abated['project_life_years'],
                                                        df_ch4_abated['capital']) + (
                                                       df_ch4_abated['opex'] - df_ch4_abated[
                                                   'reduction_volume_kt'] / methane_content *
                                                       df_ch4_abated['gas_prices_usd_per_mbtu'] * ch4_kt_to_MMBtu),
                                               -npf.pmt(discount_rate,
                                                        df_ch4_abated['project_life_years'],
                                                        df_ch4_abated['capital']) +
                                               df_ch4_abated['opex']
                                               )
    # Annual costs by kt reduced
    df_ch4_abated['dollars_kt_reduced'] = np.where(df_ch4_abated['baseline_emission'] != 0,
                                                   df_ch4_abated['annual_dollars'] /
                                                   df_ch4_abated[
                                                       'reduction_volume_kt'], 0.0)

    # Costs for the total recovered emissions
    df_ch4_abated['for_weighting'] = df_ch4_abated['recovered'] * df_ch4_abated['dollars_kt_reduced']

    # Load dataset in csv file
    df_ch4_abated.to_csv(path_to_processed_data / '4_1_vented_if_cost_data.csv', sep=';', index=False)

    return None


if __name__ == '__main__':
    pipeline()
