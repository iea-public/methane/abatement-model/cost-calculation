import importlib.util
import click
from cost_calculation.version import __version__
from cost_calculation.j_0_run_model import main


@click.group()
def cli():
    pass


@cli.command(name="version")
def version():
    click.echo(__version__)


@cli.command(name="run_model")
@click.argument("config_file", nargs=1, type=click.Path(exists=True))
def run_model(config_file):
    main(config_file)


if importlib.util.find_spec("cost_calculation.cli"):
    from cost_calculation.cli import *
