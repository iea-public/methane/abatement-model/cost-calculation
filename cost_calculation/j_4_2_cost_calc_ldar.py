from typing import List
import pandas as pd
import numpy as np
import numpy_financial as npf
from pathlib import Path

from cost_calculation.common.common.constants import *
from cost_calculation.common.common.common import load_config


# Reduction Technologies Applicability
def unpivot_redtech_applicability_by_segments(df: pd.DataFrame) -> pd.DataFrame:
    # Unpivot
    df = df.melt(id_vars=df.columns[:2],
                 value_vars=df.columns[-8:],
                 var_name='production_source',
                 value_name='applicability')

    return df


def pipeline(cfg_files: List[Path]) -> None:

    config = load_config(cfg_files)

    # File system
    # path to assumptions stored in csv file
    path_to_assumptions_data = Path(config['FILESYSTEM']['assumptions_folder'])
    # path to pre/processed folders
    path_to_preprocessed_data = Path(config['FILESYSTEM']['preprocessed_folder'])
    path_to_processed_data = Path(config['FILESYSTEM']['processed_folder'])

    # Load parameters
    df_parameters = pd.read_csv(path_to_assumptions_data / 'parameters.csv', sep=';', header=0)
    dict_parameters = dict(zip(df_parameters.parameter_name, df_parameters.parameter_value))
    discount_rate = float(dict_parameters['discount_rate'])
    ch4_at_well_ratio = float(dict_parameters['ch4_at_well_ratio'])
    ch4_in_pipeline_ratio = float(dict_parameters['ch4_in_pipeline_ratio'])

    # Load CH4 emissions by source
    df_ch4 = pd.read_csv(path_to_processed_data / '3_b_abated_emissions.csv', sep=';', header=0)

    # Fugitive
    # Reduction technology: LDAR
    df_ch4_fug = df_ch4[(df_ch4['Reason'] == 'Fugitive') &
                        (~df_ch4['Production source'].str.contains('Satellite')) &
                        (df_ch4['baseline_emission'] > 0)].reset_index(drop=True)

    df_ldar_guide = pd.read_csv(path_to_assumptions_data / 'LDARGuide.csv', sep=';', header=0)
    df_ldar_guide = df_ldar_guide[df_ldar_guide['redtech'] != 'Daily LDAR'].reset_index(drop=True)

    df_ch4_fug = df_ch4_fug.merge(df_ldar_guide,
                                  left_on=['reduction_tech_name', 'Reason'],
                                  right_on=['redtech', 'reason'],
                                  how='inner')

    df_ch4_fug['to_be_keep'] = df_ch4_fug.apply(lambda x: x.sector.lower() in x['Production source'].lower().split(),
                                                axis=1)
    df_ch4_fug = df_ch4_fug[df_ch4_fug['to_be_keep']].reset_index(drop=True)

    df_ch4_fug.drop(columns=['redtech', 'sector', 'reason', 'to_be_keep'], inplace=True)

    # Tot emissions by Production source
    df_ch4_fug_tot = df_ch4_fug.pivot_table(index=['Region', 'Reason', 'Production source'], values='baseline_emission',
                               aggfunc='sum').reset_index(drop=False)
    df_ch4_fug_tot.rename(columns={'baseline_emission': 'tot_emission'}, inplace=True)
    df_ch4_fug = df_ch4_fug.merge(df_ch4_fug_tot, on=['Region', 'Reason', 'Production source'])

    df_ch4_fug['recovered_kt'] = df_ch4_fug['baseline_emission'] * df_ch4_fug['Reduction Ratio']
    df_ch4_fug['sites_to_visit'] = df_ch4_fug['tot_emission'] / df_ch4_fug['Emissions per site (kt)']
    df_ch4_fug['number_of_leaks_to_fix'] = df_ch4_fug['recovered_kt'] / df_ch4_fug['Emissions per site (kt)']

    # Load and merge country infos
    df_ch4_fug = df_ch4_fug.merge(
        pd.read_csv(path_to_assumptions_data / 'countryGuide.csv', sep=';', header=0)[
            ['countries_name', 'tariff_scaler', 'labour', 'regions_name']].drop_duplicates(),
        left_on=['Region'],
        right_on=['countries_name'],
        how='inner')
    df_ch4_fug.drop(columns=['countries_name'], inplace=True)

    # Load and merge regional infos
    df_ch4_fug = df_ch4_fug.merge(
        pd.read_csv(path_to_assumptions_data / 'regionGuide.csv', sep=';', header=0)[
            ['regions_name', 'capital_scaler']].drop_duplicates(),
        left_on=['regions_name'],
        right_on=['regions_name'],
        how='inner')

    df_ch4_fug['training'] = (df_ch4_fug['Base Labour'] * df_ch4_fug['labour']) / df_ch4_fug['Yearly Working Hours'] * \
                             df_ch4_fug['Initial Training Hours']

    df_ch4_fug['capital'] = df_ch4_fug['Base Capital'] * df_ch4_fug['capital_scaler'] + df_ch4_fug['training']

    df_ch4_fug['Amortized Modified Capital+Training'] = -npf.pmt(discount_rate,
                                                                 df_ch4_fug['Project Life'],
                                                                 df_ch4_fug['capital'])

    df_ch4_fug['Modified Labour'] = np.where(df_ch4_fug['Is Contractor'],
                                            df_ch4_fug['Base Labour'] * df_ch4_fug['labour'] * (
                                                    1 + df_ch4_fug['Contractor Fee']),
                                            df_ch4_fug['Base Labour'] * df_ch4_fug['labour'])

    df_ch4_fug['Inspection costs (usd)'] = df_ch4_fug['Travel time'] / df_ch4_fug['Yearly Working Hours'] * df_ch4_fug[
        'sites_to_visit'] * (df_ch4_fug['Amortized Modified Capital+Training'] + df_ch4_fug['Modified Labour'])

    df_ch4_fug['Repair costs (usd)'] = df_ch4_fug['Hours to fix leak'] * df_ch4_fug[
        'number_of_leaks_to_fix'] * df_ch4_fug['Modified Labour'] / df_ch4_fug['Yearly Working Hours']

    df_ch4_fug['Total Cost (usd)'] = df_ch4_fug['Inspection costs (usd)'] + df_ch4_fug['Repair costs (usd)']

    df_gas_prices_by_region = pd.read_csv(path_to_preprocessed_data / '3_avg_gas_prices.csv', sep=';', header=0)

    df_ch4_fug = df_ch4_fug.merge(df_gas_prices_by_region[['regions_name', 'gas_prices_usd_per_mbtu']],
                                  on='regions_name',
                                  how='inner')

    df_ch4_fug['Total recovered gas (usd)'] = np.where(df_ch4_fug['Production source'].str.contains('Downstream'),
                                                       (df_ch4_fug['gas_prices_usd_per_mbtu'] * df_ch4_fug[
                                                           'recovered_kt']) * ch4_kt_to_MMBtu / ch4_in_pipeline_ratio,
                                                       (df_ch4_fug['gas_prices_usd_per_mbtu'] * df_ch4_fug[
                                                           'recovered_kt']) * ch4_kt_to_MMBtu / ch4_at_well_ratio)

    df_ch4_fug['Weighting'] = (df_ch4_fug['Total Cost (usd)'] - df_ch4_fug['Total recovered gas (usd)'])

    df_ch4_fug['Cost (usd/kt)'] = df_ch4_fug['Weighting'] / df_ch4_fug['recovered_kt']
    df_ch4_fug['Cost (usd/MBtu)'] = df_ch4_fug['Weighting'] / df_ch4_fug['recovered_kt'] / ch4_kt_to_MMBtu

    df_ch4_fug.to_csv(path_to_processed_data / '4_2_fugitive_cost_data.csv', sep=';', index=False)

    return None


if __name__ == '__main__':
    pipeline([Path('scripts/config/public.ini')])