"""
Pivot reduction technologies and their weights to table format.
Adjust weights for solar technologies based on their location.
"""
from typing import List
import pandas as pd
from pathlib import Path

from cost_calculation.common.common.common import load_config


# Reduction Technologies by source
def unpivot_redtech_by_source(df : pd.DataFrame) -> pd.DataFrame:
    # Each equipment-specific source can be abated using multiple reduction technology solutions.
    # A maximum of 5 different reduction technologies can be used for each source.
    # The weight of each technology to the total reduction is defined by the user based on expertise judgement.

    # Pivot cols to rows
    df = df.melt(
        id_vars=df.columns[0:3],
        value_vars=df.columns[3:],
        var_name='var')
    # Create ids col
    df['reduction_tech_id'] = df['var'].str.rsplit("_", 1).str[1]
    df['var'] = df['var'].str.rsplit("_", 1).str[0]

    # Pivot rows to cols
    df = df.pivot(['source', 'epa_segment', 'reason', 'reduction_tech_id'], columns='var', values='value').reset_index()

    # Remove unabated sources and none defined options
    df = df[df['reduction_tech_name'].notna()].reset_index(drop=True)

    return df


# The weight of solar reduction technologies is geographically dependent.
# In countries with a good solar exposure, the role of solar reduction technologies will be emphasised.
def weight_solar_redtech_by_localisation(df_redtech_by_source: pd.DataFrame, df_countries: pd.DataFrame,
                                         df_regions: pd.DataFrame) -> pd.DataFrame:


    # Some reduction techs are geographically dependent
    # For example: Replace Pneumatic Chemical Injection Pumps with Solar Electric Pumps

    # If the reduction tech is solar dependent, a factor taking into account the solar exposition
    # (depending of the country) will be added

    # Add regional solar factor to countries
    df_countries = df_countries.merge(df_regions[['regions_name', 'solar_factor']], on='regions_name', how='left')

    # Add countries to redtech by source
    df_redtech_by_source = df_countries.iloc[:, [0, 1, 2, 3, -1]].merge(df_redtech_by_source, how='cross')

    # Adjust redtech ratio for solar technology
    df_redtech_by_source.loc[df_redtech_by_source['reduction_tech_name'] \
                             == 'Replace Pneumatic Chemical Injection Pumps with Solar Electric Pumps',
                             'reduction_tech_ratio'] = df_redtech_by_source['solar_factor']

    df_redtech_by_source.loc[df_redtech_by_source['reduction_tech_name'] \
                             == 'Replace Pneumatic Chemical Injection Pumps with Electric Pumps',
                             'reduction_tech_ratio'] = 1 - df_redtech_by_source['solar_factor']

    df_redtech_by_source.drop(columns=['solar_factor'])

    return df_redtech_by_source


def pipeline(cfg_files: List[Path]) -> None:

    config = load_config(cfg_files)

    # File system
    # path to assumptions stored in csv file
    path_to_assumptions_data = Path(config['FILESYSTEM']['assumptions_folder'])
    # path to processed folders
    path_to_processed_data = Path(config['FILESYSTEM']['processed_folder'])

    df = weight_solar_redtech_by_localisation(
        unpivot_redtech_by_source(pd.read_csv(path_to_assumptions_data / 'redtechBySource.csv', sep=';', header=0)),
        pd.read_csv(path_to_assumptions_data / 'countryGuide.csv', sep=';', header=0),
        pd.read_csv(path_to_assumptions_data / 'regionGuide.csv', sep=';', header=0))

    df.to_csv(path_to_processed_data / '2_redtech_by_source.csv', sep=';', index=False)


if __name__ == '__main__':

    pipeline([Path('scripts/config/public.ini')])