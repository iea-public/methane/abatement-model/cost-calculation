"""
This job takes outputs from cost files (outputs from 4_1, 4_2, 4_3). It calculates unabated emissions. Then
all datasets are concatenated together, formatted and loaded to a csv file.
"""
from typing import List
import pandas as pd
import numpy as np
from pathlib import Path

from cost_calculation.common.common.common import load_config
from cost_calculation.common.common.constants import ch4_kt_to_MMBtu


def transform_vented_if_dataset(fpath: Path, fname: str) -> pd.DataFrame:

    df_ch4_abated = pd.read_csv(fpath / fname, sep=';', header=0)

    # Aggregate by reduction technology categories
    df_ch4_abated_agg_by_redtech = df_ch4_abated[
        ['Region', 'Reason', 'Production source', 'baseline_emission', 'redtech_cat', 'recovered',
         'dollars_kt_reduced', 'for_weighting']].groupby(
        by=['Region', 'Reason', 'Production source', 'redtech_cat']).agg(
        emissions_kt=('baseline_emission', 'sum'),
        volume=('recovered', 'sum'),
        cost_weighted=('for_weighting', 'sum')).reset_index(drop=False)

    # Costs by reduction technology categories in usd/kt
    df_ch4_abated_agg_by_redtech['Cost (usd/kt)'] = df_ch4_abated_agg_by_redtech['cost_weighted'] / \
                                                    df_ch4_abated_agg_by_redtech[
                                                        'volume']

    # Costs by reduction technology categories in usd/MBtu
    df_ch4_abated_agg_by_redtech['Cost (usd/MBtu)'] = df_ch4_abated_agg_by_redtech['Cost (usd/kt)'] / ch4_kt_to_MMBtu

    # Rename columns
    df_ch4_abated_agg_by_redtech.rename(columns={
        'Region': 'Country',
        'redtech_cat': 'Abatement technology',
        'volume': 'Possible savings (kt)',
        'emissions_kt': 'Emissions (kt)'
    }, inplace=True)

    return df_ch4_abated_agg_by_redtech[['Country', 'Production source', 'Reason', 'Abatement technology',
                                         'Emissions (kt)', 'Possible savings (kt)', 'Cost (usd/MBtu)']]


def transform_fugitive_ldar_dataset(fpath: Path, fname: str) -> pd.DataFrame:

    df_ch4_fug = pd.read_csv(fpath / fname, sep=';', header=0)

    print(f"Avg gas prices: {df_ch4_fug['gas_prices_usd_per_mbtu'].mean()}\n"
          f"Avg labour: {df_ch4_fug['Modified Labour'].mean()}\n"
          f"Avg capital: {df_ch4_fug['capital'].mean()}\n")

    df_ch4_fug = df_ch4_fug[
        ['Region', 'Production source', 'Reason', 'reduction_tech_name', 'baseline_emission', 'recovered_kt',
         'Cost (usd/MBtu)']]

    df_ch4_fug.rename(columns={
        'Region': 'Country',
        'reduction_tech_name': 'Abatement technology',
        'baseline_emission': 'Emissions (kt)',
        'recovered_kt': 'Possible savings (kt)'},
        inplace=True)

    return df_ch4_fug[['Country', 'Production source', 'Reason', 'Abatement technology', 'Emissions (kt)',
                       'Possible savings (kt)', 'Cost (usd/MBtu)']]


def transform_fugitive_sat_dataset(fpath: Path, fname: str) -> pd.DataFrame:

    df_ch4_fug = pd.read_csv(fpath / fname, sep=';', header=0)

    print(f"Avg gas prices: {df_ch4_fug['gas_prices_usd_per_mbtu'].mean()}\n"
          f"Avg labour: {df_ch4_fug['Modified Labour'].mean()}\n"
          f"Avg capital: {df_ch4_fug['capital'].mean()}\n")

    df_ch4_fug.rename(columns={
        'Region': 'Country',
        'baseline_emission': 'Emissions (kt)',
        'reduction_tech_name': 'Abatement technology',
        'recovered_kt': 'Possible savings (kt)'}, inplace=True)

    return df_ch4_fug[['Country', 'Production source', 'Reason', 'Abatement technology', 'Emissions (kt)',
                       'Possible savings (kt)', 'Cost (usd/MBtu)']]


def pipeline(cfg_files: List[Path]) -> None:

    config = load_config(cfg_files)

    # File system
    # path to assumptions stored in csv file
    path_to_assumptions_data = Path(config['FILESYSTEM']['assumptions_folder'])
    # path to pre/processed folders
    path_to_preprocessed_data = Path(config['FILESYSTEM']['preprocessed_folder'])
    path_to_processed_data = Path(config['FILESYSTEM']['processed_folder'])
    # path to output folder
    path_to_output = Path(config['OUTPUT_FILE']['output_folder'])
    output_filename = config['OUTPUT_FILE']['output_filename']

    # Load parameters
    df_parameters = pd.read_csv(path_to_assumptions_data / 'parameters.csv', sep=';', header=0)
    dict_parameters = dict(zip(df_parameters.parameter_name, df_parameters.parameter_value))
    discount_rate = float(dict_parameters['discount_rate'])
    ch4_at_well_ratio = float(dict_parameters['ch4_at_well_ratio'])
    ch4_in_pipeline_ratio = float(dict_parameters['ch4_in_pipeline_ratio'])

    df_macc = pd.concat([transform_vented_if_dataset(fpath=path_to_processed_data,
                                                     fname='4_1_vented_if_cost_data.csv'),
                         transform_fugitive_ldar_dataset(fpath=path_to_processed_data,
                                                         fname='4_2_fugitive_cost_data.csv'),
                         transform_fugitive_sat_dataset(fpath=path_to_processed_data,
                                                        fname='4_3_fugitive_sat_cost_data.csv')])

    # Map LDAR tech to cat
    df_macc.loc[
        (df_macc['Production source'].isin(['Satellite - downstream'])), 'Abatement technology'] = 'Downstream LDAR'
    df_macc.loc[(df_macc['Production source'].isin(['Satellite - upstream'])), 'Abatement technology'] = 'Upstream LDAR'

    df_macc.loc[(df_macc['Production source'].str.contains('Downstream')) & (df_macc['Abatement technology'].isin(
        ['Annual LDAR', 'Biannual LDAR', 'Quarterly LDAR',
         'Continuous LDAR'])), 'Abatement technology'] = 'Downstream LDAR'

    df_macc.loc[(~df_macc['Production source'].str.contains('Downstream')) & (df_macc['Abatement technology'].isin(
        ['Annual LDAR', 'Biannual LDAR', 'Quarterly LDAR',
         'Continuous LDAR'])), 'Abatement technology'] = 'Upstream LDAR'

    # Calculate Unabated emissions
    # Pull emissions by Production source
    df_dw = pd.read_csv(path_to_preprocessed_data / '3_ch4_emissions_by_production_source.csv', sep=';', header=0)
    # Remove null sources
    df_dw = df_dw[df_dw['Value'] > 0].reset_index(drop=True)[['Region', 'Production source', 'Reason', 'Value']]

    # We are going to sum up Emissions (kt) by Abatement technology by Production source
    # The total will be the total emissions which could be abatable if everything was 100% effective
    df_macc_agg = df_macc.groupby(by=['Country', 'Production source', 'Reason']).agg(
        tot_possible_savings=('Possible savings (kt)', 'sum'),
        tot_emissions=('Emissions (kt)', 'sum')).reset_index(drop=False)

    # Add a column with the total of emissions by production source
    df_macc_agg = df_macc_agg.merge(df_dw[~df_dw['Region'].isin(['World', 'European Union'])],
                                    left_on=['Country', 'Production source', 'Reason'],
                                    right_on=['Region', 'Production source', 'Reason'],
                                    how='right')

    df_macc_agg['Country'] = np.where(df_macc_agg['Country'].isna(), df_macc_agg['Region'], df_macc_agg['Country'])
    df_macc_agg['tot_possible_savings'].fillna(0, inplace=True)
    df_macc_agg['tot_emissions'].fillna(0, inplace=True)

    # Calculate the difference between the total of emissions minus the total of abatable emissions
    # the result is the unabated emissions i.e. emissions with no reduction option at all
    df_macc_agg['Emissions (kt)'] = (df_macc_agg['Value'].round(2) - df_macc_agg['tot_emissions'].round(2)).round(2)

    # A faire autrement
    df_macc_agg['Emissions (kt)'] = np.where(df_macc_agg['Emissions (kt)'] == -0.01, 0.0, df_macc_agg['Emissions (kt)'])

    df_macc_agg['Abatement technology'] = 'Unabated'

    # Remove unsued columns
    df_macc_agg.drop(columns=['tot_possible_savings', 'tot_emissions', 'Value'], inplace=True)

    # Concat emissions with reduction options and unabated emissions
    df_macc = pd.concat([df_macc, df_macc_agg])
    df_macc[['Possible savings (kt)', 'Cost (usd/MBtu)']] = df_macc[['Possible savings (kt)', 'Cost (usd/MBtu)']].fillna(0)

    # Format output

    # Add national infos
    df_macc = df_macc.merge(pd.read_csv(path_to_assumptions_data / 'countryGuide.csv', sep=';', header=0)[
                                ['countries_iso3', 'countries_name', 'regions_to_display']],
                            left_on='Country',
                            right_on='countries_name',
                            how='inner'
                            )

    # Add policies
    df_macc = df_macc.merge(pd.read_csv(path_to_assumptions_data / 'Policies.csv', sep=';', header=0),
                            on=['Abatement technology', 'Reason'],
                            how='left')

    # Add columns
    df_macc = df_macc.merge(pd.read_csv(path_to_assumptions_data / 'xref_ch4_cpfu_model.csv', sep=';', header=0)[
                                ['Hydrocarbon', 'Production source', 'Location', 'Reason']].drop_duplicates(),
                            on=['Production source', 'Reason'],
                            how='left')

    # Remove unsued columns
    df_macc.drop(columns=['Region', 'countries_name'], inplace=True)

    # Rename columns
    df_macc.rename(columns={'countries_iso3': 'ISO3',
                            'regions_to_display': 'Region',
                            'Location': 'Split',
                            'Hydrocarbon': 'Product',
                            'Cost (usd/MBtu)': 'Cost (USD/MBtu)'}, inplace=True)

    # Load dataset to csv file
    df_macc[['ISO3', 'Country', 'Region', 'Product', 'Production source', 'Split', 'Reason', 'Abatement technology',
             'Policy option', 'Emissions (kt)', 'Possible savings (kt)', 'Cost (USD/MBtu)']].to_csv(
        path_to_output / f"{output_filename}.csv", sep=';', index=False)


if __name__ == '__main__':
    pipeline([Path('scripts/config/public.ini')])