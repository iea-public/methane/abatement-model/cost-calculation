"""
This job takes the emissions by equipment-specific source and output emissions by reduction technologies
"""
from typing import List
import pandas as pd
from pathlib import Path

from cost_calculation.common.common.common import load_config


def pipeline(cfg_files: List[Path]) -> None:

    config = load_config(cfg_files)

    # File system
    # path to processed folders
    path_to_processed_data = Path(config['FILESYSTEM']['processed_folder'])

    # Load CH4 emissions by source
    df_ch4 = pd.read_csv(path_to_processed_data / '1_ch4_emissions_by_source.csv', sep=';', header=0)

    # Load reduction technologies by source
    df_redtech_by_source = pd.read_csv(path_to_processed_data / '2_redtech_by_source.csv', sep=';', header=0)

    # For vented and incomplete-flare equipment-specific source merge reduction options
    df_ch4 = df_ch4.merge(
        df_redtech_by_source[['countries_name', 'reason', 'epa_segment', 'source',
                              'reduction_tech_name', 'reduction_tech_ratio']],
        left_on=['Region', 'Reason', 'epa_segment', 'source'],
        right_on=['countries_name', 'reason', 'epa_segment', 'source'],
        how='left').reset_index(drop=True)

    # Remove unused columns
    df_ch4.drop(columns=['countries_name', 'reason'], inplace=True)

    # When nan values occur than means that the equipment-specific source has no reduction technology solution
    # (i.e. the source can not be abated)
    df_ch4['reduction_tech_ratio'].fillna(0, inplace=True)
    df_ch4['reduction_tech_name'].fillna('Unabated', inplace=True)

    # Emissions for a given equipment-specific source are distributed between reduction technology options
    # following weights defined by the user
    df_ch4['abatable_emission'] = df_ch4['source_emission'] * df_ch4['reduction_tech_ratio']

    # Abatable emissions are summed up by reduction technology
    df_ch4_by_redtech = df_ch4[df_ch4['reduction_tech_name'] != 'Unabated'].groupby(
        by=['Region', 'Reason', 'Production source', 'reduction_tech_name']).agg(
        baseline_emission=('abatable_emission', 'sum')).reset_index(drop=False)

    # Export abatable emissions by techno
    df_ch4_by_redtech.to_csv(path_to_processed_data / '3_b_abated_emissions.csv', sep=';', index=False)

    # Sum by production source the emission sources without abatement technologies.
    df_ch4_unabated = df_ch4[df_ch4['reduction_tech_name'] == 'Unabated'].groupby(
        by=['Region', 'Reason', 'Production source', 'reduction_tech_name']).agg(
        source_emission=('source_emission', 'sum')).reset_index(drop=False)

    # Rename columns
    df_ch4_unabated.rename(columns={'reduction_tech_name': 'Abatement technology',
                                    'source_emission': 'Emissions (kt)'}, inplace=True)

    # Export Emissions without a reduction technology solution
    df_ch4_unabated[['Region', 'Production source', 'Reason', 'Abatement technology', 'Emissions (kt)']].to_csv(
        path_to_processed_data / '3_a_no_tech_vented_unabated_emissions.csv', sep=';', index=False)


if __name__ == '__main__':
    pipeline([Path('scripts/config/public.ini')])