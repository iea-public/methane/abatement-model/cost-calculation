"""
EPA provides emissions for equipment-specific sources in the US (see countryGuide.csv file).
Using those inputs, the job calculates US equipment-specific emissions intensities by sector.
Then, using the sourceApplicabilityByCountry.csv file with the equipment-specific source existence for each country,
the US emissions intensities are scaled to provide emission intensities in all other countries.
"""
from typing import List
import pandas as pd
import numpy as np
from pathlib import Path

from cost_calculation.common.common.common import load_config


def us_sources_standardization_and_weighting(df: pd.DataFrame) -> pd.DataFrame:
    """
    :param df: US emissions by equipment-specific sources in thousand tons.
    source|reason|epa_segment|hydrocarbon|sector|emissions_by_source_kt
    :return: US sectoral emission ratio by equipment-specific sources. Dimensions are the same for the input.
    source|reason|epa_segment|hydrocarbon|sector|us_ratio
    """

    # Some equipment-specific sources exist for upstream onshore conventional oil/gas and upstream onshore
    # unconventional oil/gas sectors. The EPA data source doesn't give the share between conv and unconv emissions.
    # Then, here, emissions are equitably distributed between conv and unconv when "both" type exist for
    # the source
    df['emissions_by_source_kt'] = np.where(df['sector'] == 'Both',
                                            df['emissions_by_source_kt'] / 2,
                                            df['emissions_by_source_kt'])

    # Create a subset for unconv emission
    df_temp = df[df['sector'] == 'Both'].reset_index(drop=True)
    df_temp['sector'].replace('Both', 'Unconventional', inplace=True)

    # Concat the unconv emission subset to the main datatset
    df = pd.concat([df, df_temp])
    df['sector'].replace('Both', 'Conventional', inplace=True)

    del df_temp

    # Calculate the total emission by sector
    df_tot = df.groupby(by=['reason', 'hydrocarbon', 'sector']).agg(tot=('emissions_by_source_kt', 'sum')).reset_index(
        drop=False)

    # Add tot column to the main dataset
    df = df.merge(df_tot, on=['reason', 'sector', 'hydrocarbon'])

    # Calculate sectoral emission ratio for equipment-specific sources in the US
    df['us_ratio'] = df['emissions_by_source_kt'] / df['tot']

    df['us_rounded_ratio'] = df['us_ratio'].round(2)

    return df[['source', 'reason', 'epa_segment', 'hydrocarbon', 'sector', 'us_ratio']]


def unpivot_sources_by_countries(df: pd.DataFrame) -> pd.DataFrame:
    """
    :param df: equipment-specific source applicability by country
    :return: df: equipment-specific source applicability by country with countries pivoted
    """

    # Unpivot
    df = df.melt(id_vars=df.columns[:5],
                 value_vars=df.columns[5:],
                 var_name='countries_name',
                 value_name='has_source')

    df['has_source'] = df['has_source'].eq('Y').mul(1)

    return df


def pipeline(cfg_files: List[Path]) -> None:

    # Load config data
    config = load_config(list_of_cfg_files=cfg_files)

    # File system
    # path to assumptions stored in csv file
    path_to_assumptions_data = Path(config['FILESYSTEM']['assumptions_folder'])
    # path to pre/processed folders
    path_to_preprocessed_data = Path(config['FILESYSTEM']['preprocessed_folder'])
    path_to_processed_data = Path(config['FILESYSTEM']['processed_folder'])

    # US equipment-specific vented emissions intensities
    df_sources = pd.read_csv(path_to_assumptions_data / 'sourceGuide.csv', sep=';', header=0)
    df_sources_vented = us_sources_standardization_and_weighting(
        df_sources[(df_sources['reason'] == 'Vented') & (df_sources['emissions_by_source_kt'] > 0.0)].reset_index(
            drop=True))

    # Country-specific, equipment-specific vented source existence
    # Extract whole dataset from csv file
    df_sources_applicability_by_country = pd.read_csv(path_to_assumptions_data / 'sourceApplicabilityByCountry.csv',
                                                      sep=';', header=0)

    df_sources_applicability_by_country_custom = pd.read_csv(
        path_to_assumptions_data / 'sourceApplicabilityByCountryCustom.csv',
        sep=';', header=0)

    # Format dataset as a table
    df_sources_vented_applicability_by_country = unpivot_sources_by_countries(
        df_sources_applicability_by_country[df_sources_applicability_by_country['reason'] == 'Vented'].reset_index(
            drop=True))

    df_sources_applicability_by_country_custom = df_sources_applicability_by_country_custom.melt(
        id_vars=df_sources_applicability_by_country_custom.columns[:5],
        value_vars=df_sources_applicability_by_country_custom.columns[5:],
        var_name='countries_name',
        value_name='customed_ratio')

    # US source weights are recalculated for each country based on equipment-specific sources existence in the country
    df_src_weighting = df_sources_vented_applicability_by_country.merge(df_sources_vented,
                                                                 on=['source',
                                                                     'reason',
                                                                     'epa_segment',
                                                                     'hydrocarbon',
                                                                     'sector'],
                                                                 how='inner')

    # Filter : keep only existing equipment-specific sources
    df_src_weighting = df_src_weighting[df_src_weighting['has_source'] == True].reset_index(drop=True)

    # For existing sources calculate the total of emissions by sector
    df_src_weighting_tot = df_src_weighting.pivot_table(index=['countries_name'],
                                                        columns=['reason', 'hydrocarbon', 'sector'],
                                                        values='us_ratio',
                                                        aggfunc='sum').melt(ignore_index=False,
                                                                            var_name=['reason', 'hydrocarbon',
                                                                                      'sector'],
                                                                            value_name='tot').reset_index()
    # Merge the total of emissions to the emissions table
    df_src_weighting = df_src_weighting.merge(df_src_weighting_tot)

    # Remove the total of emissions
    del df_src_weighting_tot

    # For each country and equipment-specific source, calculate the intensities
    df_src_weighting['ctry_ratio'] = df_src_weighting['us_ratio'] / df_src_weighting['tot']

    # Merge custom table to previous table
    df_src_weighting = df_src_weighting.merge(
        df_sources_applicability_by_country_custom[df_sources_applicability_by_country_custom['reason'] == 'Vented'],
        on=['source', 'reason', 'epa_segment', 'hydrocarbon', 'sector',
            'countries_name'], how='right')

    # If a customed value exists, then use it
    df_src_weighting['ctry_ratio'] = np.where(df_src_weighting['customed_ratio'].notna(),
                                              df_src_weighting['customed_ratio'],
                                              df_src_weighting['ctry_ratio'])

    # Remove unused columns
    df_src_weighting.drop(columns=['customed_ratio'], inplace=True)

    # Remove nan rows
    df_src_weighting = df_src_weighting[df_src_weighting['ctry_ratio'].notna()].reset_index(drop=True)

    # Previous steps were for vented intensities only
    # For incomplete-flare and fugitive emissions country-specific equipment-specific intensities is set to 1.0

    # List non vented sources:
    df_if_fug = df_sources_applicability_by_country[
        df_sources_applicability_by_country['reason'] != 'Vented'].reset_index(drop=True)
    df_if_fug = df_if_fug[df_if_fug.columns[:5]]
    # Set source intensities to 1.0
    df_if_fug['ctry_ratio'] = 1.0
    # Country-specific intensities for fugitive and incomplete-flare sources
    df_if_fug = df_src_weighting[['countries_name']].drop_duplicates().reset_index(drop=True).merge(df_if_fug,
                                                                                                    how='cross')
    # Reconcile datasets
    df_src_weighting = pd.concat([df_src_weighting, df_if_fug])

    # Load sectoral CH4 emissions calculated by IEA WEM model
    df_ch4 = pd.read_csv(path_to_preprocessed_data / '3_ch4_emissions_by_production_source.csv', sep=';', header=0)

    # Merge country-specific equipment-specific intensities on IEA sector
    df_ch4 = df_ch4.merge(
        df_src_weighting[['countries_name', 'epa_segment', 'source', 'reason', 'sector', 'hydrocarbon', 'ctry_ratio']],
        left_on=['Region', 'Hydrocarbon', 'Sector', 'Reason'],
        right_on=['countries_name', 'hydrocarbon', 'sector', 'reason'],
        how='left').reset_index(drop=True)

    # Filter null emissions
    df_ch4 = df_ch4[df_ch4['Value'] > 0].reset_index(drop=True)

    # Remove unused columns
    df_ch4.drop(columns=['countries_name', 'hydrocarbon', 'sector', 'reason'], inplace=True)

    # Emissions by country-specific equipment-specific sources
    df_ch4['source_emission'] = df_ch4['Value'] * df_ch4['ctry_ratio']
    df_ch4['source_emission'] = df_ch4['source_emission'].apply(lambda x: round(x, 2) if x != 0 else 0.0)

    df_ch4.to_csv(path_to_processed_data / '1_ch4_emissions_by_source.csv',
                  sep=';', index=False)


if __name__ == '__main__':

    pipeline([Path('scripts/config/public.ini')])
