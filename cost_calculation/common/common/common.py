import pandas as pd
from configparser import ConfigParser
from pathlib import Path, PurePath
from typing import List


def load_config(list_of_cfg_files: List[Path]):
    # Load config data
    config = ConfigParser()
    config.read(list_of_cfg_files)
    return config


def create_folder_if_not_exists(path: Path):
    if isinstance(path, PurePath):
        path.mkdir(parents=True, exist_ok=True)
        return print(f"{path} has been created!")


def load_og_countries_list() -> List:

    # Load config data
    config = load_config(['scripts/config/public.ini'])

    # Load countries from ref file
    df_countries = pd.read_csv(
        Path(config['FILESYSTEM']['assumptions_folder']) / 'countryGuide.csv', sep=";",
        header=0)

    return list(df_countries['countries_name'].unique())


def load_og_countries_df() -> pd.DataFrame:

    # Load config data
    config = load_config(['scripts/config/public.ini'])

    # Load countries from ref file
    df_countries = pd.read_csv(
        Path(config['FILESYSTEM']['assumptions_folder']) / 'countryGuide.csv', sep=";",
        header=0)

    return df_countries
