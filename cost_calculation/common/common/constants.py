import pint

ureg = pint.UnitRegistry()

# Methane energy density
# to have 1MMBtu = 1055 MJ => 1 MJ = 0.00094787 MMBtu
# (default pint conversion factor is too accurate, 1MMBtu = 1055.056 MJ)
MJ_to_Btu = (1*ureg.Btu/round((1*ureg.Btu).to(ureg.MJ), 6))
methane_nrj_density = (35.9 * ureg.MJ) * MJ_to_Btu / ureg.meter ** 3

# Methane density
methane_density = 0.6797 * ureg.kg / ureg.meter ** 3

# Conversion factor
ch4_kt_to_MMBtu = round(methane_nrj_density * 1/methane_density)
GJ_to_kWh = round((1 * ureg.GJ).to(ureg.kWh), 2)

if __name__ == '__main__':
    print(f"GJ to kWh: {GJ_to_kWh}")