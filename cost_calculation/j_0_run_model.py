from pathlib import Path
import logging

from cost_calculation.common.common.common import load_config, create_folder_if_not_exists

from cost_calculation.j_1_calc_source_emissions_by_ctry import pipeline as \
    calc_source_emissions_by_ctry
from cost_calculation.j_2_calc_redtech_by_source import pipeline as \
    calc_redtech_by_source
from cost_calculation.j_3_calc_baseline_emission_by_redtech import pipeline as \
    calc_baseline_emission_by_redtech
from cost_calculation.j_4_1_cost_calc import pipeline as cost_calc
from cost_calculation.j_4_2_cost_calc_ldar import pipeline as cost_calc_ldar
from cost_calculation.j_4_3_cost_calc_sat import pipeline as cost_calc_sat
from cost_calculation.j_5_etl_reconcile_datasets import pipeline as etl_reconcile_datasets


def main(cfg_file: Path):

    cfg_files = [cfg_file]

    # Load config data
    config = load_config(list_of_cfg_files=cfg_files)

    # Create filesystem defined in ini files:
    for section in config.keys():
        for variable in config[section]:
            if ('folder' in variable) & (Path(config[section][variable]).exists() is False):
                create_folder_if_not_exists(Path(config[section][variable]))

    # Load path_to_logs
    path_to_logs = Path(config['FILESYSTEM']['logs_folder'])

    # Define log file
    logging.basicConfig(filename=path_to_logs / 'abatements.log',
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        level=logging.INFO)

    # Start
    logging.info('Start to calculate source emissions by country')
    calc_source_emissions_by_ctry(cfg_files)

    logging.info('Start to shape reduction technologies by source')
    calc_redtech_by_source(cfg_files)

    logging.info('Start to calculate baseline emissions')
    calc_baseline_emission_by_redtech(cfg_files)

    logging.info('Start to calculate MACC dataset for vented and incomplete flare emissions')
    cost_calc(cfg_files)

    logging.info('Start to calculate MACC dataset for fugitive emissions detected by LDAR')
    cost_calc_ldar(cfg_files)

    logging.info('Start to calculate MACC dataset for fugitive emissions detected by satellite')
    cost_calc_sat(cfg_files)

    logging.info('Start to reconcile datasets and load output')
    etl_reconcile_datasets(cfg_files)