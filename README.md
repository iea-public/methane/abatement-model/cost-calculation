# Methane Abatement model

The IEA Methane Abatement model is a tool that allows interested users to estimate oil and gas methane abatement
 potential and the associated cost of abatement by country, segment and reduction technology. 
Assumptions and inputs can be adjusted to allow users to tailor the methane abatement estimates by
 country, segment and methane reduction technology. 

This model underpins the IEA's work on methane abatement as found in the
 [Global Methane Tracker](https://www.iea.org/reports/global-methane-tracker-2024) and 
 associated [database of country and regional estimates](https://www.iea.org/data-and-statistics/data-tools/methane-tracker). 
 For information on inputs and the related methodology, 
 please read the [Global Methane Tracker documentation](https://iea.blob.core.windows.net/assets/d42fc095-f706-422a-9008-6b9e4e1ee616/GlobalMethaneTracker_Documentation.pdf).

The IEA is releasing the abatement part of its methane model publicly to support methane abatement estimations, as well
 as to give people the opportunity to review our model and provide feedback. We would be happy to receive any
  information that would help us to improve the accuracy of our model. Please contact MethaneTracker@iea.org if you
   would like to provide any relevant information.

## Table of Contents
1. [Model overview](#model-overview)
2. [Technical overview](#technical-overview)
3. [Installation and running instructions](#installation-and-running-instructions)
4. [How to contribute](#how-to-contibute)

## Model overview
The abatement model uses the IEA's best estimates of methane emissions from the oil and gas sector, by emission type, 
in each modelled region, to estimate the potential abatement potential by region and segment, as well as
 the associated cost of that abatement.

<u>**Methane (CH<sub>4</sub>) emissions estimates**</u>

The IEA's approach to estimating methane emissions from global oil and gas operations relies on generating country-specific 
and production type-specific emission intensities that are applied to production and consumption data on a country-by-country basis.

CH4 emissions are calculated for the following oil and gas sectors:
- Upstream onshore conventional oil/gas
- Upstream onshore unconventional oil/gas
- Upstream offshore oil/gas
- Downstream oil/gas

These are further segregated by type of emissions:
- **Vented methane emissions** re the result of intentional releases due to the design of the facility or equipment
 (e.g. pneumatic controllers) or operational requirements (e.g. venting a pipeline for inspection and maintenance).
- **Fugitive methane emissions** occur from leakages that are not intended, for example because of a faulty seal or leaking valve.
- **Incomplete flaring methane emissions** can occur when natural gas that cannot be used or recovered economically is burned instead of being sold or vented. 
The vast majority of the natural gas is converted into CO2 and water, but some portion is not combusted, releasing methane into the atmosphere.

For more information on the IEA's methane emissions estimates, please refer to the
 [Global Methane Tracker documentation](https://iea.blob.core.windows.net/assets/d42fc095-f706-422a-9008-6b9e4e1ee616/GlobalMethaneTracker_Documentation.pdf).

<u>**Abatement and cost estimates**</u>

These estimates are performed by this open model. It estimates the abatement potential from available methane reduction technologies and the associated costs. Therefore, it utilises 
the IEA's estimates of oil and gas methane emissions alongside estimates of regional well-head natural gas prices as starting data inputs. You can find this data at the IEA's [website](https://iea.org/data-and-statistics/data-product/methane-abatement-model).

To estimate the potential methane abatement in the oil and gas industry, the sectoral emissions need to be broken down into
equipment-specific emission sources.  The [Inventory of U.S. Greenhouse Gas Emissions and Sinks](https://www.epa.gov/ghgemissions/inventory-us-greenhouse-gas-emissions-and-sinks) (see EPA, 2024 in references for full citation)
 is used along with a wide range of other publicly-reported, credible data sources, such as scientific studies, to derive equipment-specific emissions. Each emission source can be associated with a reduction technology. Broadly, these include:
- **Replacing existing devices**, including early replacement of devices, replacing pumps, replacing with an electric motor, replacing the compressor seal or rod, and/or replacing with instrument air systems. 
- **Installing new devices**, including vapour recovery units (VRUs), blowdown capture, installing ignition devices in flares, or installing a plunger.
- **Leak detection and repair (LDAR)**, which refers to the process of locating and repairing fugitive leaks.

Associated costs and applicability factors are then based on a range of available information and expert review. Sources include the
 [US Natural Gas Start Program](https://www.epa.gov/natural-gas-star-program), 
 the [US Methane Mitigation Technologies Platform](https://www.epa.gov/natural-gas-star-program/methane-mitigation-technologies-platform),
 the [Report on Small-scale Technologies for Utilization of Associated Gas](https://documents.worldbank.org/en/publication/documents-reports/documentdetail/305891644478108245/report-on-small-scale-technologies-for-utilization-of-associated-gas?_gl=1*wwpvgx*_gcl_au*NjQxNTc0ODQwLjE3MjExMzE3NTk) and the [Economic Analysis of Methane Emission Reduction Opportunities in the U.S. Onshore Oil and Natural Gas Industries](https://www.edf.org/sites/default/files/methane_cost_curve_report.pdf).
  Every abatement option has a capital
  cost that is annualised based off the typical life span and operational costs of each technology. These costs are generally based on
   information from the United States, but labour costs, whether the equipment is imported or manufactured domestically
    (which impacts the capital costs and whether or not import taxes are levied),
 and capital costs are modified based on country-specific or region-specific information.

For more information on reduction technologies, please refer to the [Global Methane Tracker documentation](https://iea.blob.core.windows.net/assets/d42fc095-f706-422a-9008-6b9e4e1ee616/GlobalMethaneTracker_Documentation.pdf).

## Technical overview

### Model inputs: assumptions and parameters

All tunable assumptions and parameters are localised under ./data/raw/assumptions folder. 

Here is a brief description of these datasets. 
For more information, 
 please refer to the [Global Methane Tracker documentation](https://iea.blob.core.windows.net/assets/48ea967f-ff56-40c6-a85d-29294357d1f1/GlobalMethaneTracker_Documentation.pdf).

#### parameters.csv
 
 Defines assumptions of methane content and discount rate.  

![parameters](./docs/resources/parameters_table.PNG)
  
- ch4_in_pipeline_ratio: We assume a methane content of 93% for pipeline flows of natural gas
- ch4_at_well_ratio: We assume a methane content of 83% for well-head flows of natural gas
- discount_rate: The costs and revenue for each technology or abatement measure are converted into net present 
value using a discount rate of 8%

#### countryGuide.csv 

This file lists all 113 regions used in the model, with input data used to scale capital and operational costs. 

![countryGuide](./docs/resources/coutryGuide_table.PNG)

- countries_iso3: Alpha-3 code following ISO 3166 international standard
- countries_name: Name of the country (as used internally)
- regions_to_display: Name of the country's region (as it will be displayed)
- regions_name: Name of the country's region (as used internally)
- tariff_scaler: This factor takes into account whether the equipment is imported or manufactured domestically (based on information from the [World Trade Organization](http://tariffdata.wto.org/default.aspx))
- labour: This factor reflects country-level or regional estimates of labour costs (based on information from the [International Labour Organisation](https://ilostat.ilo.org/data/))
    
#### regionGuide.csv

This table contains additional regional information used to tailor abatement estimates, including costs and revenue, and is linked to the previous one using regions_name. 
  
![regionGuide](./docs/resources/regionGuide_table.PNG)

- regions_name: Name of the country's region (as used internally)
- regions_to_display: Name of the country's region (as it will be displayed)
- base: Natural gas market reference for prices: EU, US, China, Japan
- solar_factor: Regional average solar irradiance factor
- capital_scaler: Capital costs are adjusted regionally based on this factor

#### sourceGuide.csv

This file lists the equipment-specific sources by type. It is based on the sources in the [Inventory of U.S. Greenhouse Gas Emissions and Sinks](https://www.epa.gov/ghgemissions/inventory-us-greenhouse-gas-emissions-and-sinks).
  
![sourceGuide](./docs/resources/sourceGuide_table.PNG)
  
- source: Name of equipment-specific source
- reason: Type of emissions: Vented, Fugitive, Incomplete-flare
- epa_segment: Emissions segment (following the US EPA nomenclature)
- hydrocarbon: Oil or Gas
- sector: Emissions segment (following IEA nomenclature)
- emissions_by_source_kt: Annual emissions (kt) as reported by the US EPA  

 #### sourceApplicabilityByCountry.csv
 
 This defines if emissions sources are applicable to modelled regions.
  
![sourceApplicabilityByCountry](./docs/resources/sourceApplicabilityByCountry_table.PNG)
  
 #### sourceApplicabilityByCountryCustom.csv

Default values for equipment-specific sources are calculated by the model based on the US EPA data aforementioned and Y/N values set under **sourceApplicabilityByCountry.csv** file. Users can custom ratio values in this file. 
Be careful, the total of ratios for each hydrocarbon and sector must equal 100%.

![sourceApplicabilityByCountryCustom](./docs/resources/sourceApplicabilityByCountryCustom_table.PNG)

#### redtechGuide.csv 

Ths sets the amount of emissions that can be abated from each reduction technology (on a scale from 0 to 1), 
if the reduction technology needs to be imported, the base capital and operating cost, project life, 
if the project can use solar-based technology, and if the gas is salable.

![redtechGuide](./docs/resources/redtechGuide_table.PNG)
  
- redtech: Reduction technology name 
- redtech_cat: Reduction technology category 
- reason: Type of emissions: Vented, Fugitive, Incomplete-flare
- is_imported: Set to true if the equipment is likely to be imported rather than manufactured domestically
- reduction_ratio : Reduction potential of the technology
- reduction_volume_kt: Quantity of recoverable gas in kt per system deployed
- base_capital: Capital cost 
- base_operating: Operational costs 
- project_life_years: Usual time life of the project. Used to annualise capital costs. 
- is_solar_tech: Set to true if the technology runs on solar energy
- is_gas_salable: Set to true if the technology allows the recovered gas to be sold. 

#### LDARGuide.csv

Sets the abatement potential, assumed project life, travel time, and hours to fix leak of each type of LDAR.

![LDARGuide](./docs/resources/LDARGuide_table.PNG)

- redtech: Reduction technology name 
- redtech_cat: Reduction technology category
- reason: Type of emissions: Vented, Fugitive, Incomplete-flare
- Emissions per site (kt): Quantity of avoidable gas leaks in kt
- Reduction Ratio: Reduction potential of the technology
- Project Life: Usual time life of the project. Used to annualise the capial cost.
- Travel time: Time to access the leak
- Hours to fix leak: Hours to fix the leak
- Base Capital: Capital cost
- Base Labour: Operational costs
- Yearly Working Hours
- Initial Training Hours
- Is Contractor: Set to true by default 
- Contractor Fee: Set to 30% by default

#### redtechApplicability.csv

The assumed applicability of each reduction technology to oil and gas segments, on a scale from 0 to 1.
 This is to consider that some facilities or sites are not suitable to the deployment of the abatement technology.

![redtechApplicability](./docs/resources/redtechApplicability_table.PNG)
  
#### redTechBySource.csv

The source of emissions and the assumed share of emissions set to each technology (the sum of all applicable reduction technologies for one emission source must be 1). 

![redTechBySource](./docs/resources/redTechBySource_table.PNG)

#### Kayrros.csv

Estimated number and average size of emissions for available countries, based on satellite-detected methane data 
from Sentinel-5P processed by [Kayrros](https://www.kayrros.com/?gclid=CjwKCAiAjfyqBhAsEiwA-UdzJFGZ4Ll_bGZBFuuWIZo0MNYCUlXCDlBL1BulptMV2hxbWAwSO87AehoCOmQQAvD_BwE&keyword=kayrros&matchtype=e&network=g&device=c&campaign_id=19976812707&utm_source=google&utm_medium=cpc&utm_campaign=GF_Search_Brand_UK_FR&utm_content=Kayrros_Exact&utm_term=kayrros&hsa_acc=8220926764&hsa_cam=19976812707&hsa_grp=151682378761&hsa_ad=671914793720&hsa_src=g&hsa_tgt=kwd-623882924109&hsa_kw=kayrros&hsa_mt=e&hsa_net=adwords&hsa_ver=3&gad_source=1).

![Kayrros](./docs/resources/Kayrros_table.PNG)

#### Policies.csv

Sets policy options to implement abatement measures.

![Policies](./docs/resources/Policies_table.PNG)

### Description 

The model can be decomposed into two parts. 

 <u>**Stage 1**</u>
  
The first part of the model generates country-specific and production type-specific abatement potential estimates based on different reduction technologies. It uses assumptions and parameters in the csv files 
stored in ./data/raw/assumptions folder. Two other datasets are necessary as inputs to run the stage 1:
- Estimated emissions by production source csv file, 
- Estimated regional averaged gas prices in USD/MBtu for the model base year.

Those files are generated from the IEA IT infrastructure and are downloadable from the 
[IEA Website](https://www.iea.org/data-and-statistics/data-product/methane-tracker-database).
Those two files will then be stored into ./data/preprocessed folder (see 
[Installation and running instructions](#installation-and-running-instructions) section)
 
Below is an overview of Stage 1 of the model.
  
![cc_stage_1](docs/resources/pipeline_overview_cc_stage_1.png)


Modules to run this part of the model can be found in the cost_calculation package. 
The below is a description of each module 
 
  - **j_1_calc_source_emissions_by_ctry.py**: Calculates the US equipment-specific emissions ratios by sector using the split of US EPA 
 emissions for equipment-specific sources in the US (see countryGuide.csv file).
Then, using the sourceApplicabilityByCountry.csv file with the equipment-specific adjustments for each country,
the US emissions ratios are scaled to provide emission ratios in all other countries.
  - **j_2_calc_redtech_by_source.py**: Pivots the reduction technologies and their weights to table format and adjusts the weights for solar technologies based on their location.
  - **j_3_calc_baseline_emission_by_redtech.py**: Takes the emissions by equipment-specific source and outputs the emissions associated with each reduction technology.
  
  <u>**Stage 2**</u>

  The final part of the model undertakes the cost calculations and formats the dataset. 
  It uses the csv files generated by stage 1 to calculate these. Below is an overview of Stage 2 of the model.
  
![cc_stage_2](docs/resources/pipeline_overview_cc_stage_2.png)
  
The below is a description of each module.
  - **j_4_1_cost_calc.py**: Calculates the cost of implementing reduction technologies to abate vented emissions and emissions from incomplete-flaring.
  - **j_4_2_cost_calc_ldar.py**: Calculates the cost of implementing LDAR to abate fugitive emissions.
  - **j_4_3_cost_calc_sat.py**: Calculates the cost of implementing LDAR using satellite-based systems to abate very large fugitive emissions.
  - **j_5_etl_reconcile_datasets.py**: Takes outputs from cost files (outputs from 4_1, 4_2, 4_3) and calculates unabated emissions. Then 
  concatenate all datasets together, formats and loads them to a csv file.
  
  **Output file**
  - **MT_og_macc.csv**: The final output file which has the estimated emissions by country, product, production source,
  split, reason, abatement technology, and policy option along with the emissions that could be reduced
  with the abatement technology and policy option and the cost per MBtu to do so.
  
## Installation and use instructions

#### Prerequisites
- Ensure you have a functioning Python Integrated Development Environment (IDE) installed
- Install [Anaconda distribution](https://docs.anaconda.com/free/anaconda/install/index.html)
- Clone or copy the repository. For example, Git Bash (for Git users) or a Linux terminal can be used.
- In a Terminal, navigate to the folder of the model, and follow the following instructions:

        # First create an isolated environement to run the program
        conda env create -f environment.yml
    
        # Then, activate the environment
        conda activate cost_calculation
        
        # And run:
        pip install -e .
        
        # Then, go to ./config folder and open the abatement.ini
        # We encourage you to keep the default architecture, however, in this file you can redefine the filesystem
        
        # Go on IEA website and download input files and unzip them under ./data/preprocessed folder (create the preprocessed 
        # folder if necessary)
        
        # You can modify assumptions which will input the model under ./data/raw/assumptions folder
        
        # Finally, go to the root of the project and run the command:
        cost_calculation run_model ./config/abatement.ini
  
## How to contribute
There is a wide range of assumptions in this model that are based on available information and best estimates of country-specific data.
Please contact MethaneTracker@iea.org if you would like to provide any relevant information.
The IEA would be happy to review feedback on any inputs or assumptions within the model to help us to improve the accuracy of our model.

## Acknowledgement

The IEA’s preferred attribution is:

IEA Methane Abatement Model, IEA, https://gitlab.com/iea-public/methane/abatement-model/cost-calculation,
License: [source code:  GNU GPL v3.0 or datasets:  CC BY SA 4.0].

## License

The IEA Methane Abatement model is available under a GNU GPL v3.0 license for code source and CC BY SA 4.0 for datasets. 

Please see [LICENSE](LICENSE) for details.

Please also note the following:
-	if the OECD/IEA cannot settle amicably with you any disputes relating to the model, either you or the OECD/IEA may, pursuant to a notice of arbitration communicated by reasonable means to the other, elect to have the dispute referred to and finally determined by arbitration. The arbitration shall be conducted in accordance with the Arbitration Rules of the United Nations Commission on International Trade Law (UNCITRAL) then in force. The arbitral tribunal shall consist of a sole arbitrator and the language of the proceedings shall be English unless otherwise agreed. The place of arbitration shall be Paris. The arbitral proceedings shall be conducted remotely (e.g., via telephone conference or written submissions) whenever practicable. The parties shall be bound by any arbitration award rendered as a result of such arbitration as the final adjudication of such a dispute; and
-	Nothing in the licensing terms referred to above constitutes or may be interpreted as a limitation upon, or waiver of, any privileges and immunities that apply to the OECD/IEA or you, all of which are specifically reserved.


## References

EPA (2024). Inventory of U.S. Greenhouse Gas Emissions and Sinks: 1990-2022 U.S. Environmental Protection Agency, EPA 430R-24004. https://www.epa.gov/ghgemissions/inventory-us-greenhouse-gas-emissions-and-sinks-1990-2022.
